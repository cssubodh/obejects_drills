var _ = require('underscore');
function invert(testObject) {
    return _.invert(testObject);
}
module.export = {invert};