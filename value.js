var _ = require('underscore');
function values(testObject) {
    return _.values(testObject);
}
module.export = {values};